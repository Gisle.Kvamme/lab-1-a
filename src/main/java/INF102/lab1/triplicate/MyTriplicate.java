package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        
        HashMap<T, Integer> map = new HashMap<T, Integer>();

        for (T t : list) {
            if (!map.containsKey(t))
                map.put(t, 1);
            else 
                map.put(t, map.get(t)+1);
            

            if (map.get(t) == 3)
                return t; 
        }
        return null;
    }
}
